People
======

.. toctree::
   :maxdepth: 2

	Robert Mercer  <People/Robert-Mercer>
	Alexander Nix  <People/Alexander-Nix>
	Steve Bannon   <People/Steve-Bannon.rst>
    Nigel Oakes    <People/Nigel-Oakes>