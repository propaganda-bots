Robert Mercer 
=============

Background
----------

:doc: `../files/articles/Robert Mercer’s Secret Adventure as a New Mexico Cop.txt`


From `wikipedia article`_

Brexit
------
- Andy Wigmore, communications director of Leave.EU, said that Mercer donated the services of data analytics firm Cambridge Analytica to Nigel Farage, the head of the United Kingdom Independence Party. The firm was able to advise Leave.EU through its ability to harvest data from people's Facebook profiles in order to target them with individualized persuasive messages to vote for Brexit. However, Leave.EU did not inform the UK electoral commission of the donation despite the fact that a law demands that all donations valued over £7,500 must be reported.[30]

2016 U.S. election
------------------

- By January 2016 Mercer was the biggest single donor in the 2016 U.S. presidential race.[6]
- In June 2016, he was ranked the #1 donor to federal candidates in the 2016 election cycle as he had donated $2 million to John R. Bolton's super PAC and $668,000 to the Republican National Committee.[20]
- Mercer was a major financial supporter of the 2016 presidential campaign of Ted Cruz,[31] contributing $11 million to a super PAC associated with the candidate.[32]
- Mercer was a major supporter of Donald Trump's 2016 campaign for president.[7]
- Mercer and his daughter Rebekah helped to get Steve Bannon and Kellyanne Conway senior roles in the Trump campaign.[25]
- Rebekah worked with Conway on the Cruz Super-PAC Keep the Promise in the 2016 Republican primaries.[10]
- Mercer also financed a Super PAC, Make America Number One, which supported Trump's campaign.[25]
- Nick Patterson, a former colleague of Mercer's said in 2017 that Trump would not have been elected without Mercer's support.[14]

Race relations
--------------
- Mercer has said that the Civil Rights Act of 1964, the landmark federal statute arising from the civil rights movement of the 1960s, was a major mistake.
- In 2017, David Magerman, a former Renaissance employee, alleged in a lawsuit that Mercer had said that African Americans were economically better off before the civil rights movement, that white racists no longer existed in the United States, and that the only racists remaining were black racists.[14]


.. _wikipedia article: https://en.wikipedia.org/wiki/Robert_Mercer
