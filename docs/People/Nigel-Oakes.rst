Nigel Oakes
===========

CEO, SCL Group - Behavioural Influence
London, United Kingdom 

Contact Info
	:Profile: linkedin.com/in/nigel-oakes-fri-99b8177
	:Websites:
	    - scl.cc  (Company Website)
	    - bdinstitute.org  (Company Website)
	:Birthday: July 3

from `linkedin`_

	Nigel Oakes is a British political scientist whose ideas have laid the foundation for many significant developments both in military influence and population analysis. He is the CEO of SCL Group, and the Chairman of the Behavioural Dynamics Institute.

	Specialties: Strategic Influence, Behavioural Marketing, Behavioural Target Audience Analysis, AB-MOE Infrastructure, Public Diplomacy Policy and Soft Power. 


Experience

    Behavioural Dynamics Institute
    	:Role: Chairman
	    :Dates Employed: Jul 1989 – Present
	    :Employment: Duration 29 yrs 4 mos
	    :Location: London, United Kingdom
	    :Duties:
	    	- Overall management of institution and chairman of grant funding committee.

    SCL Group - Strategic Communication Laboratories
    	:Role: CEO
    	:Dates Employed: 2004 – Jun 2018
    	:Employment Duration: 14 yrs
    	:Duties:
	    	- Providing advanced influence strategies to governments based on scientific Target Audience Analysis. 
    		- Providing influence training programmes to governments & militaries internationally.
    		- Behavioural research for commercial companies.
    		- Behaviour change projects for social programmes

    Strategic Communication Laboratories International
	    :Role: CEO
	    :Dates Employed: Jul 1995 – Feb 2003
	    :Employment Duration: 7 yrs 8 mos
    	:Duties:
			- SCL International was the Internationally based corporate vehicle of the current SCL Group

	Ellipse Digitan and Delta Project
    	:Role: Partner
    	:Dates Employed: 1999 – 2001
    	:Employment Duration: 2 yrs
    	:Duties:
    		- Project Director

    Harrington Oakes
    	:Role: CEO
	    :Dates Employed: Feb 1990 – Mar 1995
	    :Employment Duration: 5 yrs 2 mos
    	:Location: London, United Kingdom
    	:Duties:
    		- Managed a full service communication and production company for international clients. The company specialised in managing difficult 'people problems' and we handled a number of industrial disputes, strikes and worker malaise. The company also specialised in designing communication for IPO's for the merchant banking community.

Education

    National Broadcasting School
    	Field Of Study Broadcasting
    	Grade Diploma
    	Dates attended or expected graduation 1981 – 1982
    
    Dartington School of Music
    	Field Of Study Music
    	Dates attended or expected graduation 1980 – 1981
    
    Eton College
    	Dates attended or expected graduation 1976 – 1980


Articles:
	- `Why is ex adman Nigel Oakes being hailed as the 007 of big data`_ 



.. _linkedin: https://www.linkedin.com/in/nigel-oakes-fri-99b8177/
.. _Why is ex adman Nigel Oakes being hailed as the 007 of big data: https://economictimes.indiatimes.com/why-is-ex-adman-nigel-oakes-being-hailed-as-the-007-of-big-data/articleshow/57872909.cms
