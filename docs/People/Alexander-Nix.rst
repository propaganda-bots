
NIX, Alexander James Ashburner

:Correspondence addresses: 
	- 55 Baker Street, London, W1U 7EU
	- 13 St James's Gardens, London, United Kingdom, W11 4RD  
:Date of birth: May 1975 
:Nationality: British
:Channels:
	- `linkedin`_ 


Work
----

Information from `companieshouse gov uk`_

- 6 January 2015 - now
    Ceo/Director, SCL DIGITAL LIMITED (09375055)
		:Company status: Dissolved

- 6 January 2015 - now
    Ceo/Director, SCL SOVEREIGN LIMITED (09375809)
	    United Kingdom 

- 6 January 2015 - 30 April 2018
	Ceo/Director, CAMBRIDGE ANALYTICA(UK) LIMITED (09375920)
		:Company status: In Administration 
    	United Kingdom 


- 17 October 2012 - 30 April 2018 
	Director, SCL ELECTIONS LIMITED (08256225)
		United Kingdom 


- 19 February 2013 - 30 April 2018 
	Director, SCL SOCIAL LIMITED (08410560)
		:Company status: In Administration 
    	United Kingdom 

- 10 January 2014 - 30 April 2018
    Director, SCL COMMERCIAL LIMITED (08840965)
    	United Kingdom 

- 28 January 2016 - 30 April 2018
	Director, SCL GROUP LIMITED (05514098)
	    United Kingdom

- 23 October 2015 - 30 April 2018
    Ceo, SCL ANALYTICS LIMITED (09838667)
		United Kingdom 

- 23 January 2018 - 28 March 2018,
	Director/Ceo, EMERDATA LIMITED (10911848)
	:Correspondence address:
	    Pkf Littlejohn, 1 Westferry Circus, Canary Wharf, London, England, E14 4HD

- 7 March 2018 - 7 March 2018, 
	Director, FIRECREST TECHNOLOGIES LIMITED (11238956) 
		United Kingdom 

Education
    - Eton College 1988 – 1993
    - The University of Manchester


    

.. _companieshouse gov uk: https://beta.companieshouse.gov.uk/officers/YEplkqBMfc6Rp3dbPs57pN3aqO4/appointments
.. _linkedin: https://www.linkedin.com/in/alexander-nix-aa57142
