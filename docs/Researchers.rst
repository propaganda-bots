My Regards
==========

.. toctree::
   :maxdepth: 2

		Digital Intelligence Lab 	<Researchers/Digital-Intelligence-Lab>
		The Computational Propaganda Project <Researchers/The-Computational-Propaganda-Project.rst>


Researchers / Artists
---------------------

- `Samuel Woolley`_ `twitter <https://twitter.com/samuelwoolley>`_
- `Paul Virilio`_
- `Lombardi`_
- `Adam Harvey`_

Papers
------

- :download:`Computational Propaganda in the USA: Manufacturing Consensus Online <files/papers/Comprop-USA.pdf>`

- :download:`The Biology of Disinformation <files/papers/IFTF_biology_of_disinformation_062718.pdf>`

- :download:`State Sponsored Trolling <files/papers/IFTF_State_sponsored_trolling_report.pdf>`

- :download:`News and Political Information Consumption in Brazil <files/papers/News-and-Political-Information-Consumption-in-Brazil.pdf>`

- :download:`Political Communication, Computational Propaganda, and Autonomous Agents <files/papers/Political-Communication-Computational-Propaganda-and-Autonomous-Agents.pdf>`

- :download:`Computational Propaganda Slides <files/papers/Computational-Propaganda-Slides.pdf>`

Projects
--------

Critical projects questioning the new form of journalism and misinformation: 
   - `misinfocon <https://london.misinfocon.com/>`_
   - `hackshackers <https://hackshackers.com/>`_


.. _Paul Virilio: https://en.wikipedia.org/wiki/Paul_Virilio
.. _Samuel Woolley: https://samwoolley.org/
.. _Lombardi: https://en.wikipedia.org/wiki/Mark_Lombardi
.. _Adam Harvey: https://ahprojects.com/


