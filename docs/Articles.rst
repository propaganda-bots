Articles
========

 - [The great British Brexit robbery][3]: how our democracy was hijacked ( Sun 7 May 2017 )

 - [Stop worrying about fake news. What comes next will be much worse][5] 


[3]: https://www.theguardian.com/technology/2017/may/07/the-great-british-brexit-robbery-hijacked-democracy
[5]: https://www.theguardian.com/commentisfree/2016/dec/09/fake-news-technology-filters
