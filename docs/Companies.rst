Companies
=========

.. toctree::
   :maxdepth: 2

	AggregateIQ 		<Companies/AggregateIQ>
	CAMBRIDGE ANALYTICA <Companies/CAMBRIDGE-ANALYTICA>
	DATA PROPRIA		<Companies/DATA-PROPRIA>
	SCL Group Limited	<Companies/SCL-Group>
	Emeralda			<Companies/Emeralda>
	Palandir			<Companies/Palandir>
	Behavioural Dynamics Institute <Companies/Behavioural-Dynamics-Institute>
	The Messina Group 	<Companies/The-Messina-Group>
	Endless Gain		<Companies/Endless-Gain>
