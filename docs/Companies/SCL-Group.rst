SCL Group
=========

*global election management agency*
 

:Formerly: SCL GROUP LIMITED
:Previous company names: STRATEGIC COMMUNICATION LABORATORIES LIMITED (20 Jul 2005 - 19 Aug 2015)
:Former type: Private
:Industry: Data mining, data analysis
:Founded: 1990; 28 years ago as Behavioural Dynamics Institute
Founder	Nigel John Oakes
:Defunct: 1 May 2018
:Headquarters: London, United Kingdom
:Area served: Worldwide
:Key people:
	- Nigel John Oakes
	- Roger Michael Gabb
	- Alexander Nix
	- Julian David Wheatland
:Subsidiaries:
	- Cambridge Analytica
:Website: `sclgroup`_
:Registered office address: 4 Mount Ephraim Road, Tunbridge Wells, Kent, TN1 1EE 

People:
	Director:
		- WHEATLAND, Julian David
			:Correspondence address: 4 Mount Ephraim Road, Tunbridge Wells, Kent, TN1 1EE 
			:Role: Director (Active ) 
			:Date of birth: July 1961 
			:Appointed on: 20 December 2007 
			:Nationality: British
			:Country of residence: England 
			:Occupation: Ceo
	People with significant control:
		- Mr Roger Michael Gabb Active
			:Correspondence address: Dairy House, Woodland Hall, Glazeley, Bridgnorth, Shropshire, United Kingdom, WV16 6AB 
			:Notified on: 29 September 2016 
			:Date of birth: November 1938
			:Nationality: British 
			:Nature of control:
				- Ownership of shares – More than 25% but not more than 50% 
				- Ownership of voting rights - More than 25% but not more than 50% 

	Resigned:
		- OAKES, Nigel John, Director ( 3 October 2005 - 5 June 2018 )
		- NIX, Alexander James Ashburner, Director ( 28 January 2016 - 30 April 2018 )
		- GABB, Roger Michael, Director ( 10 November 2005 - 28 March 2018 )
		- OAKES, Alexander Waddington, Secretary ( 31 March 2014 - 28 January 2016 ) 
		- OAKES, Alexander Waddington, Director ( 20 July 2005 - 28 January 2016  )
		- BOTTOMLEY, John Michael, Secretary ( 20 July 2005 - 31 March 2014 )
		- SDG SECRETARIES LIMITED, Nominee Secretary ( 20 July 2005 - 20 July 2005 )
		- CHRISTIE, David Ferguson, Director ( 12 January 2007 - 20 December 2007 )
		- NIX, Alexander James Ashburner, Director, ( 20 July 2005 - 7 December 2012 )
		- PATTIE, Geoffrey Edwin, Director ( 10 November 2005 - 1 December 2008 )
		- WHEATLAND, Julian, Ceo ( 10 November 2005 - 12 January 2007 )
		- SDG REGISTRARS LIMITED, Nominee Director ( 20 July 2005 - 20 July 2005 )





.. _sclgroup: https://sclgroup.cc