Cambridge Analytica
===================

Company Information:
:Channels:
1. `youtube`_

:Web: https://cambridgeanalytica.org
:Twitter: https://twitter.com/camanalytica
:Name: Cambridge Analytica Ltd
:Successor:  Emerdata
:Founded: 2013
:Defunct: 1 May 2018
:Headquarters: London, England
:Parent Company: `SCL Group <https://sclgroup.cc>`_ (`wiki <https://en.wikipedia.org/wiki/SCL_Group>`_).
:Key people:
  - Alexander Nix (CEO)[1]
  - Robert Mercer (investor)[2]
  - Rebekah Mercer (investor)
  - Steve Bannon (vice president, former)[3]
:Companies Related:
  - Cambridge Analytica (UK) Limited,
  - SCL Group Limited,
  - SCL Analytics Limited,
  - SCL Commercial Limited,
  - SCL Social Limited and SCL Elections Limited (together “the Companies”)
:Contact:
   Address: Crowe Clark Whitehill LLP, 4 Mount Ephraim Road, Tunbridge Wells, Kent TN1 1EE
   E-mail: recoverysolutions@crowecw.co.uk
:Offices:
  - London, 55 New Oxford Street, London, WC1A 1BS
  - New York, 597 5th Avenue, 7th Floor, New York, NY 10017
  - Washington DC, 1901 Pennsylvania Ave NW, Suite 902, Washington, DC 20006
:Divisions: 
  - [CA-political][2]
  - [CA-commercial][3]
:Emails:
  - press@cambridgeanalytica.org
  - info@cambridgeanalytica.org


From Guardian_:

  ...
  The company, SCL Elections, went on to be bought by Robert Mercer, a secretive hedge fund billionaire, renamed Cambridge Analytica, and achieved
  a certain notoriety as the data analytics firm that played a role in both Trump and Brexit campaigns. But all of this was still to come. London 
  in 2013 was still basking in the afterglow of the Olympics. Britain had not yet Brexited. The world had not yet turned.

Articles:
  - `The government hired several murky companies plying the same methods as Cambridge Analytica in their election campaign`_


Work
----

  - Yemen `Cambridge Analyticas Parent Company Used Yemen as Psyop Test Site`_




Christopher Wylie
-----------------

twitter: https://twitter.com/chrisinsilico
wikipedia: https://en.wikipedia.org/wiki/Christopher_Wylie


Aleksandr Kogan
---------------

Aleksandr Kogan (born 1985/86), who has also briefly used the name Dr Spectre,[3] is a Moldovan-born data scientist, who is known for having developed the app that allowed Cambridge Analytica to collect personal details of 80 million Facebook users. He works as a research associate at the University of Cambridge.

wikipedia: https://en.wikipedia.org/wiki/Aleksandr_Kogan


Whistleblowing
--------------

[Timeline of events][4] published by CA


[1]: https://cambridgeanalytica.org/
[2]: https://ca-political.com/
[3]: https://ca-commercial.com/
[4]: https://ca-commercial.com/news/timeline-events

.. _Guardian: https://www.theguardian.com/technology/2017/may/07/the-great-british-brexit-robbery-hijacked-democracy
.. _The government hired several murky companies plying the same methods as Cambridge Analytica in their election campaign: https://kittysjones.wordpress.com/tag/behavioural-dynamics-institute/

.. _youtube: https://www.youtube.com/channel/UCITg74vP1bNNHw3qt8DUpiQ

.. _Cambridge Analyticas Parent Company Used Yemen as Psyop Test Site: https://www.mintpressnews.com/cambridge-analyticas-yemen-psyop/245280/
