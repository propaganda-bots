Emerdata
========

[Company][1]
Part of [SCL Group][6] 

:Company number: 10911848 
:Registered office address: Pkf Littlejohn 1 Westferry Circus, Canary Wharf, London, England, E14 4HD 
:Incorporated on: 11 August 2017 
:Children Company: Firecrest

People
	[Officers][2]
		- MERCER, Jennifer
			Correspondence address
			    Cdl Family Office, 505 S Flagler Drive, Suite 900, West Palm Beach, Florida 33401, United States 
			Role: Director (Active) 
			Date of birth: May 1972 
			Appointed on: 16 March 2018 
			Nationality: American 
			Country of residence: United States 
			Occupation: Director
		- MERCER, Rebekah Anne 
			Correspondence address
			    597, 5th Avenue, 7th Floor, New York, Ny 10017, United States 
			Role: Director (Active)
			Date of birth: December 1973 
			Appointed on: 16 March 2018 
			Nationality: American 
			Country of residence: United States 
			Occupation: Director
		- TIU, Gary Ka Chun
			Correspondence address
			    Pkf Littlejohn, 1 Westferry Circus, Canary Wharf, London, England, E14 4HD 
			Role: Director (Active) 
			Date of birth: July 1977 
			Appointed on: 12 June 2018 
			Nationality: Australian 
			Country of residence: Hong Kong 
			Occupation: Company Director 
		-  WHEATLAND, Julian David 
	[Persons with significant control][4]
		- Dr Alexander Bruce Tayler (Ceased)
			Correspondence address
			    16 Great Queen Street, London, United Kingdom, WC2B 5DG 
			Notified on: 11 August 2017 
			Ceased on: 23 January 2018 
			Date of birth: September 1984 
			Nationality: British,Australian 
			Nature of control
			    Ownership of shares – More than 25% but not more than 50% 
			    Ownership of voting rights - More than 25% but not more than 50% 
			Country of residence: England 
		- Mr Julian David Wheatland (Ceased)
			Correspondence address
			    16 Great Queen Street, London, United Kingdom, WC2B 5DG 
			Notified on: 11 August 2017 
			Ceased on: 23 January 2018 
			Date of birth: July 1961 
			Nationality: British 
			Nature of control
			    Ownership of shares – More than 25% but not more than 50% 
			    Ownership of voting rights - More than 25% but not more than 50% 
			Country of residence: England 


### General

*Rebekah and Jennifer Mercer, daughters of hedge fund tycoon [Robert Mercer][5]*


### Articles

- [links between Cambridge Analytica and Emerdata][3]




[1]: https://beta.companieshouse.gov.uk/company/10911848
[2]: https://beta.companieshouse.gov.uk/company/10911848/officers
[3]: 'files/Questions over links between Cambridge Analytica and Emerdata.html'
[4]: https://beta.companieshouse.gov.uk/company/10911848/persons-with-significant-control
[5]: https://en.wikipedia.org/wiki/Robert_Mercer
[6]: https://en.wikipedia.org/wiki/SCL_Group#Emerdata_Limited