DATA PROPRIA 
============

From [Wikipedia][1]
*According to the Associated Press, a company run by former officials at Cambridge Analytica, Data Propria, has been quietly working for President Donald Trump’s 2020 re-election effort.[37]*

Company
-------

 Type: Subsidiary
 Predecessor: Cambridge Analytica
 Founded: February 2018
 Headquarters: San Antonio, Texas
 Key people:
  - Matt Oczkowski (President)[1]
  - Brad Parscale (director and part owner of parent company CloudCommerce, Inc.)
 Parent:	CloudCommerce, Inc.
 Website: [datapropria.com][2]


Data Propria is a company formed in 2018. It is managed by Cambridge Analytica's former head of product, **Matt Oczkowski**, and employs at least three other former Cambridge Analytica staffers including Cambridge Analytica's former chief data scientist, **David Wilkinson**.
It is reportedly working on the 2020 Donald Trump presidential campaign.[8] 

	
[1]: https://en.wikipedia.org/wiki/Cambridge_Analytica
[2]: https://datapropria.com
