The Messina Group
=================

:Channels:
	- `facebook`_ 
	- `twitter`_
	- `linkedin`_


from `kittysjones`_

However, in 2017, the Conservatives used several similar shadowy private companies that peddle data analytics, psychological profiling and ‘behavioural change’  to research, canvass, advertise and target message voters with ‘strategic communications’ – which also exploit their psychological characteristics and tendencies. ... Messina Group Inc were also paid £544,153.57 for transport, advertising, market research and canvassing. This company uses data analytics and ‘intelligence’ services. 

`Services`_

	The Messina Group has proven experience working across five continents to help political parties, governments, corporations, and NGOs achieve their goals with strategies that are data-driven and measurable.  

.. _Services: https://themessinagroup.com/services/
.. _facebook: https://www.facebook.com/messinagroupconsulting/
.. _twitter: https://twitter.com/MGConsulting_
.. _linkedin: https://www.linkedin.com/company/messina-group-consulting/
.. _kittysjones: https://kittysjones.wordpress.com/tag/behavioural-dynamics-institute/
