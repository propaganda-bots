Behavioural Dynamics Institute
==============================

:established in: 1989
:web: http://www.bdinstitute.org/
:Channels:
	- `vimeo`_
	- `youtube`_
:Affiliations:
    - Strategic Communication Laboratories
    - SCL Elections
:People:
    - Nigel Oakes

from `powerbase`_

	*the leading international centre of excellence for research and development into persuasion and social influence. The output of the institute’s work is fundamental to the effective application of public diplomacy, information operations, civil contingency and all aspects of strategic communication.*

.. _vimeo: https://vimeo.com/user10364855
.. _youtube: https://www.youtube.com/watch?v=f7lkG-BaCRE
.. _powerbase: http://powerbase.info/index.php/Behavioural_Dynamics_Institute

