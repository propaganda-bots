PROPAGANDA BOTS
===============


	*Propaganda bots striking thirsty for flesh and blood, desperate as they are.*


Research on Companies, People, Cases, Source Code, Researchers, States that are involved in so called **computational propaganda** - contemporary propaganda practices involving strategic use of tech.


.. toctree::
   :maxdepth: 2

   Adversaries <docs/Adversaries> 
   Companies <docs/Companies>
   People <docs/People>
   Histogram <docs/Histogram>
   Articles <docs/Articles>
   Video/Audio <docs/Video-Audio>
   Researchers <docs/Researchers>


[4]: https://www.palantir.com/
[6]: https://en.wikipedia.org/wiki/SCL_Group#Emerdata_Limited
[7]: https://sclgroup.cc/home
[8]: https://en.wikipedia.org/wiki/SCL_Group
