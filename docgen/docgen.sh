#!/bin/bash

area=$1

while read p; do
  section=`echo ${p//[[:blank:]]/} | tr '[:upper:]' '[:lower:]'`

  section=`echo $section | cut -d'/' -f1`

  echo "   $p <${area}/${section}>" >> ${area}.rst
  echo $p > ${area}/${section}.rst

  N=${#p}
  myvar=`perl -e "print '=' x $N;"`
  echo $myvar >> ${area}/${section}.rst
done <${area}.list
