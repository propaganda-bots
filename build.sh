#!/bin/sh

SPHINXOPTS=
SPHINXBUILD=python -msphinx
SPHINXPROJ="Reality Show"
SOURCEDIR=.
BUILDDIR=_build

python3 -m pip install sphinx --user

# python3 -m sphinx -M help "${SOURCEDIR}" "${BUILDDIR}" ${SPHINXOPTS}

python3 -m sphinx -M html ${SOURCEDIR} ${BUILDDIR} ${SPHINXOPTS}

